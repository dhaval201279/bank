package com.innotech.management.action;

import java.math.BigDecimal;

import org.openxava.actions.SaveAction;
import org.openxava.util.Is;

import com.innotech.management.model.BankAccount;
import com.innotech.management.model.BankAccountHistory;
import com.innotech.management.model.Constants.InstrumentType;
import com.innotech.management.model.Constants.TransactionType;
import com.innotech.management.model.Constants.TransationMethod;

public class BankAccountHistoryAction extends SaveAction {

	@Override
	public void execute() throws Exception {
		super.execute();
		// check if any validation so far processing cheque
		if (getErrors() == null || getErrors().isEmpty()) {
			// continue with bank account update & history
			try {
				String bankAccountID = getRequest().getParameter("bankAccount.id");
				if ("cheque".equalsIgnoreCase(getModelName())) {
					// always debit
					BigDecimal chequeValue = new BigDecimal(convertStringToDouble(getRequest().getParameter("chequeValue")));
					BankAccount ba = BankAccount.updateBankAccount(bankAccountID, chequeValue.multiply(new BigDecimal(-1)));
					BankAccountHistory.createBankAccountHistory(chequeValue, TransationMethod.Cheque, TransactionType.DEBIT, ba);
				} else if ("payinslip".equalsIgnoreCase(getModelName())) {
					// always credit
					BigDecimal amount = new BigDecimal(convertStringToDouble(getRequest().getParameter("amount")));
					BankAccount ba = BankAccount.updateBankAccount(bankAccountID, amount);
					BankAccountHistory.createBankAccountHistory(amount, TransationMethod.Pay_In, TransactionType.CREDIT, ba);
				} else if ("passbook".equalsIgnoreCase(getModelName())) {
					// can be credit or debit
					BigDecimal amount = new BigDecimal(convertStringToDouble(getRequest().getParameter("amount")));
					int transactionType = Integer.valueOf(getRequest().getParameter("transactionType"));
					TransactionType type = TransactionType.values()[transactionType];
					BankAccount ba = BankAccount.updateBankAccount(bankAccountID, TransactionType.DEBIT.equals(type) ? amount.multiply(new BigDecimal(-1)) : amount);
					BankAccountHistory.createBankAccountHistory(amount, TransationMethod.PassBook, type , ba);
				}
			} catch (Exception e) {
				addError("error_bankaccount_history");
			}
		}
	}
	
	private Double convertStringToDouble(String inputStr){
		if(Is.emptyString(inputStr)){
			return 0d;
		}
		inputStr = inputStr.replaceAll(",","");
		return Double.valueOf(inputStr);
	}
}
