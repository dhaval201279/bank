/**
 * 
 */
package com.innotech.management.action;

import java.util.Collection;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.openxava.actions.SaveAction;
import org.openxava.jpa.XPersistence;

import com.innotech.management.model.Company;
import com.innotech.management.model.DematAccount;
import com.innotech.management.model.StockMaster;

/**
 * @author dhaval
 * 
 *         This is responsible for creating/updating stock master of each user
 *         whenever stock is purchased or sold
 * 
 */
public class StockMasterManagementAction extends SaveAction {

	/**
	 * 
	 */
	public StockMasterManagementAction() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute() throws Exception {
		super.execute();
		try {
			if (getErrors().isEmpty()) {
				String companyCodeId = (String) this.getRequest().getParameter("companyCode.id");
				String dematAccountId = (String) this.getRequest().getParameter("dematAccount.id");
				String quantity = (String) this.getRequest().getParameter("quantity");
				String transactionType = (String) this.getRequest().getParameter("transactionType");
				Collection<StockMaster> sms = fetchStockMaster(companyCodeId, dematAccountId);
				if (sms != null && sms.isEmpty()) {
					createStockMaster(companyCodeId, dematAccountId, quantity);
				} else {
					updateStockMaster(quantity, transactionType, sms);
				}
			}
		} catch (Exception nre) {
			addError("error_stockaccount_history");
		}
	}

	private Collection<StockMaster> fetchStockMaster(String companyCodeId, String dematAccountId) {
		Query query = XPersistence.getManager()
				.createQuery(
						"select sm from StockMaster sm where " + "sm.companyCode.id = :companyCodeId AND "
								+ "sm.dematAccount.id = :dematAccountId");
		query.setParameter("companyCodeId", companyCodeId);
		query.setParameter("dematAccountId", dematAccountId);

		Collection<StockMaster> sms = (Collection<StockMaster>) query.getResultList();
		return sms;
	}

	private void createStockMaster(String companyCodeId, String dematAccountId, String quantity) {
		StockMaster sm = new StockMaster();

		Company companyCode = new Company();
		companyCode.setId(companyCodeId);

		DematAccount da = new DematAccount();
		da.setId(dematAccountId);

		sm.setQuantity(Integer.valueOf(quantity));
		sm.setCompanyCode(companyCode);
		sm.setDematAccount(da);
		// save stock master to db
		XPersistence.getManager().persist(sm);
	}

	private void updateStockMaster(String quantity, String transactionType, Collection<StockMaster> sms) {
		StockMaster sm = sms.iterator().next();
		System.out.println("stock master is not null -- sm.getQuantity : " + sm.getQuantity());
		// Purchase
		if ("0".equals(transactionType)) {
			sm.setQuantity(sm.getQuantity() + Integer.valueOf(quantity));
		}
		// Sell
		else {
			sm.setQuantity(sm.getQuantity() - Integer.valueOf(quantity));
		}
		// update stock master db
		XPersistence.getManager().merge(sm);
	}

}
