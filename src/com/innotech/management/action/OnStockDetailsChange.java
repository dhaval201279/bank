package com.innotech.management.action;

import java.math.BigDecimal;
import java.text.Bidi;

import org.openxava.actions.OnChangePropertyBaseAction;

public class OnStockDetailsChange extends OnChangePropertyBaseAction {

	public void execute() throws Exception {
		try {
			// TODO Auto-generated method stub
			BigDecimal faceValue = (BigDecimal) getView().getValue("faceValue");
			BigDecimal commission = (BigDecimal) getView().getValue("commission");
			BigDecimal tax = (BigDecimal) getView().getValue("tax");
			Integer quan = getView().getValueInt("quantity");
			BigDecimal add = faceValue.multiply(new BigDecimal(quan)).add(tax).add(commission);
			if("sale".equalsIgnoreCase(getView().getValueString("transactionType"))){
				add = add.multiply(new BigDecimal(-1));
			}
			getView().setValue("total", add);
		} catch (Exception e) {
			addError("invalid_data_for_stock_value");
		}
	}

}
