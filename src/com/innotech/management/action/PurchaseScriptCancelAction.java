package com.innotech.management.action;

import org.openxava.actions.ViewBaseAction;
import org.openxava.model.meta.MetaModel;

import com.innotech.management.model.ScriptSelectionDecision;


public class PurchaseScriptCancelAction extends ViewBaseAction {

	public void execute() throws Exception {
		getView().setModel(new ScriptSelectionDecision());
	}
	
}
