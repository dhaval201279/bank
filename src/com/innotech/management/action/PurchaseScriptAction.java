package com.innotech.management.action;

import java.util.Collection;
import java.util.Map;

import javax.persistence.Query;

import org.openxava.actions.ViewBaseAction;
import org.openxava.jpa.XPersistence;
import org.openxava.model.MapFacade;
import org.openxava.model.meta.MetaModel;
import org.openxava.util.Is;

import com.innotech.management.model.BankAccount;
import com.innotech.management.model.ScriptSelectionDecision;
import com.innotech.management.model.StockMaster;


public class PurchaseScriptAction extends ViewBaseAction {

	public void execute() throws Exception {
		Map values = getView().getValues();
		MetaModel mm = org.openxava.model.meta.MetaModel.getForPOJOClass(ScriptSelectionDecision.class);
		try {
			if(values.get("familyGroup")==null  || Is.empty(((Map)values.get("familyGroup")).get("oid"))){
				addError("script_selection_family_group_required");
			}
			
			if(values.get("companyCode") == null  || Is.empty(((Map)values.get("companyCode")).get("id"))){
				addError("script_selection_companycode_required");
			}
			
			if(getErrors().isEmpty()){
				ScriptSelectionDecision model = new ScriptSelectionDecision();
				fetchBankAccountDetails(model,values, mm);
				fetchDematAccountDetails(model,values, mm);
				getView().setModel(model);
			}
		} catch (Exception e) {
			addError("error_occurred_bank_application");
		}
		
	}

	private void fetchBankAccountDetails(ScriptSelectionDecision model,Map values, MetaModel mm) {
		String familyGroupID = String.valueOf(((Map)values.get("familyGroup")).get("oid"));
		Query query = XPersistence.getManager().createQuery("select ba from BankAccount ba join ba.firstName fn join fn.familyGroup fg where fg.oid=:id order by ba.balance desc");
		query.setParameter("id", familyGroupID);
		Collection<BankAccount> value = (Collection<BankAccount>) query.getResultList();
		mm.fillPOJO(model,values);
		model.setPurchase(value);
	}
	
	private void fetchDematAccountDetails(ScriptSelectionDecision model,Map values, MetaModel mm) {
		String familyGroupID = String.valueOf(((Map)values.get("familyGroup")).get("oid"));
		String companyCodeId  = String.valueOf(((Map)values.get("companyCode")).get("id"));
		Query query = XPersistence.getManager().createQuery("select sm from StockMaster sm  join sm.dematAccount dm join dm.firstName fn join fn.familyGroup fg where sm.companyCode.id = :companyCodeId AND fg.oid=:familyGroupId order by sm.quantity desc ");
		query.setParameter("companyCodeId", companyCodeId);
		query.setParameter("familyGroupId", familyGroupID);
		Collection<StockMaster> value = (Collection<StockMaster>) query.getResultList();
		mm.fillPOJO(model,values);
		model.setStocks(value);
	}
	
}
