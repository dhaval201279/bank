package com.innotech.management.validator;

import java.util.Calendar;
import java.util.Date;

import org.openxava.util.Messages;
import org.openxava.validators.IPropertyValidator;
import org.openxava.validators.IWithMessage;

public class NoPreviousMonthDateValidator implements IPropertyValidator, IWithMessage { 

	private String message = "no_previous_monthdate_validation"; 
	
	public void validate(Messages errors, Object value, String propertyName, String modelName) throws Exception{
		try {
			Date dt = (Date)value;
			Calendar receivedDate = Calendar.getInstance();
			receivedDate.setTime(dt);
			if(receivedDate.get(Calendar.MONTH)!= Calendar.getInstance().get(Calendar.MONTH)){
				errors.add(message, propertyName, modelName); 
			}
		}
		catch (ClassCastException ex) {
			errors.add("expected_type", propertyName, modelName, "caracter");
		}
	}
	
	public void setMessage(String message){
		this.message = message;
	}
	
}
