package com.innotech.management.validator;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openxava.calculators.ICalculator;

public class BankCurentDateCalculator implements ICalculator{

	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	
	public Object calculate() throws Exception {
		return sdf.format(new Date());
		
	}
	
	public static void main(String[] args) throws Exception{
		BankCurentDateCalculator ob= new BankCurentDateCalculator();
		System.out.println(ob.calculate());
	}
}
