package com.innotech.management.model;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.FinderException;
import javax.ejb.ObjectNotFoundException;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.Digits;
import org.openxava.annotations.DescriptionsList;
import org.openxava.annotations.Hidden;
import org.openxava.annotations.LabelFormat;
import org.openxava.annotations.LabelFormatType;
import org.openxava.annotations.ListProperties;
import org.openxava.annotations.NoCreate;
import org.openxava.annotations.NoModify;
import org.openxava.annotations.ReadOnly;
import org.openxava.annotations.Required;
import org.openxava.annotations.Tab;
import org.openxava.annotations.View;
import org.openxava.jpa.XPersistence;
import org.openxava.model.MapFacade;

@View(members = "Account Info [accountNumber ; accountType , bank ];  "
		+ "Account Holders [firstName;secondName;thirdName];"
		+ "History [bankAccountHistorys];")
@Entity
@Tab(properties="accountNumber,accountType,bank.ifsCode,bank.branchName,firstName.name,balance+")
public class BankAccount {
	
	public BankAccount() {
		this.balance = new BigDecimal(0);// TODO Auto-generated constructor stub
	}


	
	@Id
	@Hidden
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid" , strategy="uuid")
	@Column(length=32)
	private String id;
	
	@Column(length=20,unique=true)
	@Required
	private String accountNumber;
	
	@Column(length=20)
	@Required
	private Constants.AccountType accountType;
	
	@ManyToOne(fetch=FetchType.EAGER,optional=false)
	@DescriptionsList(descriptionProperties="ifsCode,branchName,bankName",order="bankName asc")
	@Required
	private Bank bank;

	@ManyToOne(fetch=FetchType.EAGER,optional=true)
	@DescriptionsList(descriptionProperties="name,familyGroup.description")
	@Required
	private FamilyMember firstName;
	
	@ManyToOne(fetch=FetchType.EAGER,optional=true)
	@DescriptionsList(descriptionProperties="name,familyGroup.description")
	private FamilyMember secondName;
	
	@ManyToOne(fetch=FetchType.EAGER,optional=true)
	@DescriptionsList(descriptionProperties="name,familyGroup.description")	
	private FamilyMember thirdName;
	
	@Digits(fractionalDigits=2, integerDigits = 15) @Hidden 
	private BigDecimal balance;
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Constants.AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(Constants.AccountType accountType) {
		this.accountType = accountType;
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public FamilyMember getFirstName() {
		return firstName;
	}

	public void setFirstName(FamilyMember firstName) {
		this.firstName = firstName;
	}

	public FamilyMember getSecondName() {
		return secondName;
	}

	public void setSecondName(FamilyMember secondName) {
		this.secondName = secondName;
	}

	public FamilyMember getThirdName() {
		return thirdName;
	}

	public void setThirdName(FamilyMember thirdName) {
		this.thirdName = thirdName;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public static BankAccount updateBankAccount(String bankAccountID, BigDecimal valueToAdd) throws ObjectNotFoundException, FinderException {
		// fetch fresh instance from DB
		Map baKeyValues = new HashMap();
		baKeyValues.put("id", bankAccountID);
		BankAccount ba = (BankAccount) MapFacade.findEntity("BankAccount", baKeyValues);
	
		// update bank account balance and persist back
		Map values = new HashMap();
		values.put("balance", ba.getBalance().add(valueToAdd));
		MapFacade.setValues("BankAccount", baKeyValues, values);
		return ba;
	}
	
	
	@NoCreate
	@NoModify
	@ReadOnly
	@ListProperties(value="method , type , transactionValue,balance , timestamp")
	public Collection<BankAccountHistory> getBankAccountHistorys(){
		Query query = XPersistence.getManager().createQuery("select bah from BankAccountHistory bah  where bah.bankAccount.id = :bankAccountId order by timestamp desc");
		query.setParameter("bankAccountId", getId());
		Collection<BankAccountHistory> bahs = (Collection<BankAccountHistory>) query.getResultList();
		return bahs;
	}
	
}
