package com.innotech.management.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.Digits;
import org.openxava.annotations.DefaultValueCalculator;
import org.openxava.annotations.DescriptionsList;
import org.openxava.annotations.Hidden;
import org.openxava.annotations.PropertyValidator;
import org.openxava.annotations.PropertyValue;
import org.openxava.annotations.Required;
import org.openxava.annotations.Stereotype;
import org.openxava.annotations.Tab;
import org.openxava.annotations.View;
import org.openxava.calculators.CurrentDateCalculator;
import org.openxava.calculators.CurrentTimestampCalculator;

import com.innotech.management.validator.NoPreviousMonthDateValidator;

@View(members = "Cheque Info [payeeName ,"+ "chequeType , chequeDate ];  "
	+ " Other [chequeNo,chequeValue,bankAccount;remarks];")
@Entity
@Tab(properties="payeeName;bankAccount.accountNumber,bankAccount.bank.branchName,bankAccount.bank.ifsCode , bankAccount.firstName.name,amount,date")
public class Cheque {
	
	public Cheque() throws Exception {
		this.timestamp = (Date)new CurrentTimestampCalculator().calculate();
	}

	@Id
	@Hidden
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid" , strategy="uuid")
	@Column(length=32)
	private String id;
	
	@Required
	@Column(length=40)
	private String payeeName;
	
	@Required
	@Column(length=20)
	private Constants.ChequeType chequeType;
	
	@Required
	@DefaultValueCalculator(CurrentDateCalculator.class)
	@PropertyValidator(NoPreviousMonthDateValidator.class)
	private Date chequeDate;
	
	@Required
	@Column(length=30)
	private String chequeNo;
	
	@Digits(fractionalDigits=2, integerDigits = 15) @Required 
	@DefaultValueCalculator(
			value=org.openxava.calculators.BigDecimalCalculator.class,
			properties={ @PropertyValue(name="value", value="0") })
	private BigDecimal chequeValue;
	
	@ManyToOne(fetch=FetchType.EAGER,optional=true)
	@DescriptionsList(descriptionProperties="bank.ifsCode,bank.branchName,accountNumber,firstName.name")
	@Required
	private BankAccount bankAccount;

	@Column(length=100)
	@Stereotype(value="TEXT_AREA")
	private String remarks;
	

	@Hidden
	@DefaultValueCalculator(CurrentTimestampCalculator.class)
	private Date timestamp;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPayeeName() {
		return payeeName;
	}

	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	public Constants.ChequeType getChequeType() {
		return chequeType;
	}

	public void setChequeType(Constants.ChequeType chequeType) {
		this.chequeType = chequeType;
	}

	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getChequeNo() {
		return chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public BigDecimal getChequeValue() {
		return chequeValue;
	}

	public void setChequeValue(BigDecimal chequeValue) {
		this.chequeValue = chequeValue;
	}

	public BankAccount getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
	
}
