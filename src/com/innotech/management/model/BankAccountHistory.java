package com.innotech.management.model;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import javax.ejb.CreateException;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.Digits;
import org.openxava.annotations.DefaultValueCalculator;
import org.openxava.annotations.DescriptionsList;
import org.openxava.annotations.Hidden;
import org.openxava.annotations.NoCreate;
import org.openxava.annotations.NoModify;
import org.openxava.annotations.ReadOnly;
import org.openxava.annotations.Tab;
import org.openxava.calculators.CurrentTimestampCalculator;
import org.openxava.jpa.XPersistence;
import org.openxava.model.MapFacade;
@Entity
@Tab(properties="bankAccount.accountNumber,bankAccount.bank.bankName,bankAccount.bank.branchName, method , type , transactionValue,balance , timestamp")
public class BankAccountHistory {
	
	
	@Id
	@Hidden
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid" , strategy="uuid")
	@Column(length=32)
	private String id;
	
	@ManyToOne(fetch=FetchType.EAGER,optional=false)
	@DescriptionsList(descriptionProperties="accountNumber")
	@ReadOnly
	private BankAccount bankAccount;
	
	@Column(length=20)
	private Constants.TransationMethod method;
	
	@Column(length=20)
	private Constants.TransactionType type;
	
	@Digits(fractionalDigits=2, integerDigits = 15) 
	private BigDecimal transactionValue;

	@Digits(fractionalDigits=2, integerDigits = 15) 
	private BigDecimal  balance;

	@DefaultValueCalculator(CurrentTimestampCalculator.class)
	private Date timestamp;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BankAccount getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}

	public Constants.TransationMethod getMethod() {
		return method;
	}

	public void setMethod(Constants.TransationMethod method) {
		this.method = method;
	}

	public Constants.TransactionType getType() {
		return type;
	}

	public void setType(Constants.TransactionType type) {
		this.type = type;
	}

	public BigDecimal getTransactionValue() {
		return transactionValue;
	}

	public void setTransactionValue(BigDecimal transactionValue) {
		this.transactionValue = transactionValue;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public static void createBankAccountHistory(BigDecimal value, Constants.TransationMethod method , Constants.TransactionType type, BankAccount ba) throws Exception,CreateException {
		// create vlaues map for bank account history
		Map bahMap = new HashedMap();
		Map idMap = new HashedMap();
		idMap.put("id", ba.getId());
		bahMap.put("bankAccount", idMap);
		bahMap.put("method", method);
		bahMap.put("type",type);
		bahMap.put("transactionValue", value);
		bahMap.put("balance", ba.getBalance());
		bahMap.put("timestamp",new CurrentTimestampCalculator().calculate());
		BankAccountHistory bah = (BankAccountHistory) MapFacade.create("BankAccountHistory", bahMap);
	}
	
	
	
}
