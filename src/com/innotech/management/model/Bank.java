/**
 * 
 */
package com.innotech.management.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;
import org.openxava.annotations.Hidden;
import org.openxava.annotations.LabelFormat;
import org.openxava.annotations.Required;
import org.openxava.annotations.RowStyle;
import org.openxava.annotations.Tab;
import org.openxava.annotations.View;

/**
 * @author dhaval
 * 
 */
@Entity
@View(members = "Bank Info [bankName , " + "branchName];  "
		+ "Bank Address [branchAddress1;" + "branchAddress2;" + "city,"
		+ "state , pin];" + "Other [ifsCode, contactNumber];")
@Tab(properties = "bankName,branchName,ifsCode,city")
public class Bank {

	@Id
	@Hidden
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(length = 32)
	private String oid;

	@Column(length = 30)
	@Required
	private String bankName;

	@Column(length = 30)
	@Required
	private String branchName;

	@Column(length = 30)
	@Required
	private String branchAddress1;

	@Column(length = 30)
	private String branchAddress2;

	@Column(length = 20)
	@Required
	private String city;

	@Column(length = 20)
	@Required
	private Constants.State state;

	@Column(length = 6)
	@Required
	private Integer pin;

	@Column(length = 20)
	@Required
	private String ifsCode;

	@Column(length = 12)
	@Required
	private String contactNumber;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "bank")
	private Set<BankAccount> bankAccounts;

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}

	/**
	 * @param bankName
	 *            the bankName to set
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	/**
	 * @return the branchName
	 */
	public String getBranchName() {
		return branchName;
	}

	/**
	 * @param branchName
	 *            the branchName to set
	 */
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	/**
	 * @return the branchAddress1
	 */
	public String getBranchAddress1() {
		return branchAddress1;
	}

	/**
	 * @param branchAddress1
	 *            the branchAddress1 to set
	 */
	public void setBranchAddress1(String branchAddress1) {
		this.branchAddress1 = branchAddress1;
	}

	/**
	 * @return the branchAddress2
	 */
	public String getBranchAddress2() {
		return branchAddress2;
	}

	/**
	 * @param branchAddress2
	 *            the branchAddress2 to set
	 */
	public void setBranchAddress2(String branchAddress2) {
		this.branchAddress2 = branchAddress2;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	public Constants.State getState() {
		return state;
	}

	public void setState(Constants.State state) {
		this.state = state;
	}

	public Integer getPin() {
		return pin;
	}

	public void setPin(Integer pin) {
		this.pin = pin;
	}

	public String getIfsCode() {
		return ifsCode;
	}

	public void setIfsCode(String ifsCode) {
		this.ifsCode = ifsCode;
	}

	/**
	 * @return the contactNumber
	 */
	public String getContactNumber() {
		return contactNumber;
	}

	/**
	 * @param contactNumber
	 *            the contactNumber to set
	 */
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public Set<BankAccount> getBankAccounts() {
		return bankAccounts;
	}

	public void setBankAccounts(Set<BankAccount> bankAccounts) {
		this.bankAccounts = bankAccounts;
	}

}