package com.innotech.management.model;

public class Constants {



public enum TransationMethod{
	Cheque,
	PassBook,
	Pay_In
}

public enum InstrumentType {
	CHEQUE,
	DEMAND_DRAFT,
	CASH
}

public enum TransactionType {
	DEBIT,
	CREDIT
}

public enum ChequeType {AC_PAY , BEARER}

public enum AccountType{
	Savings , Current , OD , Recurrring,Other
}

public enum State {
	Delhi,
	Mumbai,
	Gujarat, 
	MadhyaPradesh {
		@Override
		public String toString() {
			return "Madhya Pradesh";
		}
	}
}

public enum DematAccountType { NSDL , CSDL}

public enum StockTransactionType {Purchase ,Sale}
}