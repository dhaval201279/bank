package com.innotech.management.model;

import java.util.Collection;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Query;

import org.hibernate.annotations.GenericGenerator;
import org.openxava.annotations.Hidden;
import org.openxava.annotations.NoCreate;
import org.openxava.annotations.NoModify;
import org.openxava.annotations.ReadOnly;
import org.openxava.annotations.Required;
import org.openxava.jpa.XPersistence;

@Entity
public class FamilyGroup {
	@Id
	@Hidden
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid" , strategy="uuid")
	@Column(length=32)
	private String oid;
	
	@Column(length=50)
	@Required
	private String description;

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	@NoCreate
	@NoModify
	@ReadOnly
	public Collection<FamilyMember> getFamilyMembers(){
		Query query = XPersistence.getManager().createQuery("select fm from FamilyMember fm where fm.familyGroup.id = :familyGroupId");
		query.setParameter("familyGroupId", getOid());
		Collection<FamilyMember> familyMemebers = (Collection<FamilyMember>) query.getResultList();
		return familyMemebers;
	}
}
