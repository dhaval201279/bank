/**
 * 
 */
package com.innotech.management.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.Digits;
import org.openxava.annotations.DefaultValueCalculator;
import org.openxava.annotations.DescriptionsList;
import org.openxava.annotations.Hidden;
import org.openxava.annotations.PropertyValidator;
import org.openxava.annotations.PropertyValue;
import org.openxava.annotations.Required;
import org.openxava.annotations.Stereotype;
import org.openxava.annotations.Tab;
import org.openxava.annotations.View;
import org.openxava.calculators.CurrentDateCalculator;
import org.openxava.calculators.CurrentTimestampCalculator;

import com.innotech.management.validator.NoPreviousMonthDateValidator;

/**
 * @author dhaval
 *
 */
@View(members = "Primary Info [bankAccount; date;transactionType;amount;remarks];")
		
@Entity
@Tab(properties="transactionType,bankAccount.accountNumber,bankAccount.bank.branchName,bankAccount.bank.ifsCode , bankAccount.firstName.name,amount,date")

public class Passbook {

	/**
	 * 
	 */
	public Passbook() throws Exception {
		this.timestamp = (Date)new CurrentTimestampCalculator().calculate();
	}

	@Id
	@Hidden
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid" , strategy="uuid")
	@Column(length=32)
	private String id;
	 
	
	@ManyToOne(fetch=FetchType.EAGER,optional=true)
	@DescriptionsList(descriptionProperties="bank.ifsCode,bank.branchName,accountNumber,firstName.name")
	@Required
	private BankAccount bankAccount;

	@Required
	@DefaultValueCalculator(CurrentDateCalculator.class)
	@PropertyValidator(NoPreviousMonthDateValidator.class)
	private Date date;
	
	@Digits(fractionalDigits=2, integerDigits = 15) @Required 
	@DefaultValueCalculator(
			value=org.openxava.calculators.BigDecimalCalculator.class,
			properties={ @PropertyValue(name="value", value="0") })
	private BigDecimal amount;
	
	@Column(length = 10) @Required
	private Constants.TransactionType transactionType;

	@Column(length=100)
	@Stereotype(value="TEXT_AREA")
	private String remarks;
	

	@Hidden
	@DefaultValueCalculator(CurrentTimestampCalculator.class)
	private Date timestamp;
	
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}


	public BankAccount getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the transactionType
	 */
	public Constants.TransactionType getTransactionType() {
		return transactionType;
	}

	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(Constants.TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

}
