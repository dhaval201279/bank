package com.innotech.management.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.openxava.annotations.DescriptionsList;
import org.openxava.annotations.Hidden;
import org.openxava.annotations.Required;
import org.openxava.annotations.Tab;
import org.openxava.annotations.View;

@View(members = "Account Info [accountCode ; serviceProvider ; dematAccountType , DPIT ];  "
	+ "Account Holders [firstName;secondName;thirdName];")
@Entity
@Tab(properties="accountCode,firstName.name,serviceProvider,dematAccountType")
public class DematAccount {

	@Id
	@Hidden
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid" , strategy="uuid")
	@Column(length=32)
	private String id;
	
	@Required
	@Column(length=20)
	private Long accountCode;
	
	@Required
	@Column(length=50,name="servierProvider")
	private String serviceProvider;
	
	@Required
	@Column(length=20)
	private Constants.DematAccountType dematAccountType;
	
	@Required
	@Column(length=30)
	private String DPIT;
	
	@ManyToOne(fetch=FetchType.EAGER,optional=true)
	@DescriptionsList(descriptionProperties="name,familyGroup.description")
	@Required
	private FamilyMember firstName;
	
	@ManyToOne(fetch=FetchType.EAGER,optional=true)
	@DescriptionsList(descriptionProperties="name,familyGroup.description")
	private FamilyMember secondName;
	
	@ManyToOne(fetch=FetchType.EAGER,optional=true)
	@DescriptionsList(descriptionProperties="name,familyGroup.description")
	private FamilyMember thirdName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(Long accountCode) {
		this.accountCode = accountCode;
	}

	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	public Constants.DematAccountType getDematAccountType() {
		return dematAccountType;
	}

	public void setDematAccountType(Constants.DematAccountType dematAccountType) {
		this.dematAccountType = dematAccountType;
	}

	public String getDPIT() {
		return DPIT;
	}

	public void setDPIT(String dPIT) {
		DPIT = dPIT;
	}

	public FamilyMember getFirstName() {
		return firstName;
	}

	public void setFirstName(FamilyMember firstName) {
		this.firstName = firstName;
	}

	public FamilyMember getSecondName() {
		return secondName;
	}

	public void setSecondName(FamilyMember secondName) {
		this.secondName = secondName;
	}

	public FamilyMember getThirdName() {
		return thirdName;
	}

	public void setThirdName(FamilyMember thirdName) {
		this.thirdName = thirdName;
	}
	
	
}
