package com.innotech.management.model;

import java.math.BigDecimal;
import java.util.Collection;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Query;

import org.hibernate.validator.Digits;
import org.openxava.annotations.Action;
import org.openxava.annotations.DefaultValueCalculator;
import org.openxava.annotations.DescriptionsList;
import org.openxava.annotations.ListProperties;
import org.openxava.annotations.NoCreate;
import org.openxava.annotations.NoModify;
import org.openxava.annotations.NoSearch;
import org.openxava.annotations.OnChange;
import org.openxava.annotations.PropertyValue;
import org.openxava.annotations.ReadOnly;
import org.openxava.annotations.Required;
import org.openxava.annotations.View;
import org.openxava.jpa.XPersistence;

import com.innotech.management.action.OnStockDetailsChange;

@View(members = "familyGroup;" + "Stock To Search [companyCode;quantity,faceValue;commission,tax;total];" + " Account Info { purchase };" + "Stock Info { stocks }")
public class ScriptSelectionDecision {

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@DescriptionsList(order = "description asc")
	@Required
	@NoCreate
	@NoModify
	private FamilyGroup familyGroup;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@DescriptionsList(order = "companyCode asc")
	@Required
	@NoCreate
	@NoModify
	private Company companyCode;

	@Required
	@DefaultValueCalculator(value = org.openxava.calculators.IntegerCalculator.class, properties = { @PropertyValue(name = "value", value = "0") })
	@OnChange(OnStockDetailsChange.class)
	private int quantity;

	@Digits(fractionalDigits = 2, integerDigits = 5)
	@Required
	@DefaultValueCalculator(value = org.openxava.calculators.BigDecimalCalculator.class, properties = { @PropertyValue(name = "value", value = "0") })
	@OnChange(OnStockDetailsChange.class)
	private BigDecimal faceValue;

	@Digits(fractionalDigits = 2, integerDigits = 5)
	@Required
	@DefaultValueCalculator(value = org.openxava.calculators.BigDecimalCalculator.class, properties = { @PropertyValue(name = "value", value = "0") })
	@OnChange(OnStockDetailsChange.class)
	private BigDecimal commission;

	@Digits(fractionalDigits = 2, integerDigits = 5)
	@Required
	@DefaultValueCalculator(value = org.openxava.calculators.BigDecimalCalculator.class, properties = { @PropertyValue(name = "value", value = "0") })
	@OnChange(OnStockDetailsChange.class)
	private BigDecimal tax;

	@Digits(fractionalDigits = 2, integerDigits = 15)
	@DefaultValueCalculator(value = org.openxava.calculators.BigDecimalCalculator.class, properties = { @PropertyValue(name = "value", value = "0") })
	private BigDecimal total;

	@NoCreate
	@NoModify
	@ReadOnly
	@ListProperties("accountNumber,accountType,bank.ifsCode,bank.bankName,bank.branchName,balance")
	private Collection<BankAccount> purchase;
	
	
	@NoCreate
	@NoModify
	@ReadOnly
	@ListProperties("dematAccount.accountCode,dematAccount.firstName.name,companyCode.companyCode,quantity")
	private Collection<StockMaster> stocks;
	
	public FamilyGroup getFamilyGroup() {
		return familyGroup;
	}

	public void setFamilyGroup(FamilyGroup familyGroup) {
		this.familyGroup = familyGroup;
	}

	public Company getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(Company companyCode) {
		this.companyCode = companyCode;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getFaceValue() {
		return faceValue;
	}

	public void setFaceValue(BigDecimal faceValue) {
		this.faceValue = faceValue;
	}

	public Collection<BankAccount> getPurchase() {
		return purchase;
	}

	public void setPurchase(Collection<BankAccount> purchase) {
		this.purchase = purchase;
	}

	public BigDecimal getCommission() {
		return commission;
	}

	public void setCommission(BigDecimal commission) {
		this.commission = commission;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Collection<StockMaster> getStocks() {
		return stocks;
	}

	public void setStocks(Collection<StockMaster> stocks) {
		this.stocks = stocks;
	}

}
