/**
 * 
 */
package com.innotech.management.model;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

import javax.ejb.CreateException;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Query;

import org.apache.commons.collections.map.HashedMap;
import org.hibernate.annotations.GenericGenerator;
import org.openxava.annotations.DefaultValueCalculator;
import org.openxava.annotations.DescriptionsList;
import org.openxava.annotations.Hidden;
import org.openxava.annotations.ListProperties;
import org.openxava.annotations.NoCreate;
import org.openxava.annotations.NoModify;
import org.openxava.annotations.PropertyValue;
import org.openxava.annotations.ReadOnly;
import org.openxava.annotations.Required;
import org.openxava.annotations.Tab;
import org.openxava.calculators.CurrentTimestampCalculator;
import org.openxava.jpa.XPersistence;
import org.openxava.model.MapFacade;
import org.openxava.util.Is;

/**
 * @author dhaval
 *
 */
@Entity
@Tab(properties="dematAccount.accountCode, dematAccount.firstName.name,companyCode.companyCode,quantity")
public class StockMaster {

	/**
	 * 
	 */
	public StockMaster() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@Hidden
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(length = 32)
	private String id;


	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@DescriptionsList(descriptionProperties = "accountCode")
	@Required
	@ReadOnly
	private DematAccount dematAccount;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@DescriptionsList(order = "companyCode asc")
	@Required
	@ReadOnly
	private Company companyCode;

	@Column(length = 5)
	@Required
	@ReadOnly
	//@OnChange(OnStockDetailsChange.class)
	@DefaultValueCalculator(value = org.openxava.calculators.IntegerCalculator.class, properties = { @PropertyValue(name = "value", value = "0") })
	private int quantity;


	@NoCreate
	@NoModify
	@ReadOnly
	@ListProperties(value="dematAccount.accountCode,dematAccount.firstName.name,companyCode.companyCode,companyCode.description,quantity+,total+,dateOfPruchaseOrSale")
	public Collection<Stock> getStocks(){
		if(!Is.empty(getCompanyCode()) && !Is.empty(getDematAccount())){
			Query query = XPersistence.getManager().createQuery("select st from Stock st where " + "st.companyCode.id = :companyCodeId AND st.dematAccount.id = :dematAccountId");
			query.setParameter("companyCodeId", getCompanyCode().getId());
			query.setParameter("dematAccountId", getDematAccount().getId());
			Collection<Stock> sts = (Collection<Stock>) query.getResultList();
		return sts;
		}
		return null;
	}
	
	
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the companyCode
	 */
	public Company getCompanyCode() {
		return companyCode;
	}

	/**
	 * @param companyCode the companyCode to set
	 */
	public void setCompanyCode(Company companyCode) {
		this.companyCode = companyCode;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the dematAccount
	 */
	public DematAccount getDematAccount() {
		return dematAccount;
	}

	/**
	 * @param dematAccount the dematAccount to set
	 */
	public void setDematAccount(DematAccount dematAccount) {
		this.dematAccount = dematAccount;
	}
	
	public static void createStockMaster(BigDecimal value, Constants.TransationMethod method , Constants.TransactionType type, BankAccount ba) throws Exception,CreateException {
		// create vlaues map for bank account history
		Map bahMap = new HashedMap();
		Map idMap = new HashedMap();
		idMap.put("id", ba.getId());
		bahMap.put("bankAccount", idMap);
		bahMap.put("method", method);
		bahMap.put("type",type);
		bahMap.put("transactionValue", value);
		bahMap.put("balance", ba.getBalance());
		bahMap.put("timestamp",new CurrentTimestampCalculator().calculate());
		BankAccountHistory bah = (BankAccountHistory) MapFacade.create("BankAccountHistory", bahMap);
	}
	
}
