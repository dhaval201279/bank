/**
 * 
 */
package com.innotech.management.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.Digits;
import org.openxava.annotations.DefaultValueCalculator;
import org.openxava.annotations.DescriptionsList;
import org.openxava.annotations.Editor;
import org.openxava.annotations.Hidden;
import org.openxava.annotations.OnChange;
import org.openxava.annotations.PropertyValue;
import org.openxava.annotations.Required;
import org.openxava.annotations.Tab;
import org.openxava.annotations.View;
import org.openxava.calculators.CurrentDateCalculator;

import com.innotech.management.action.OnStockDetailsChange;
import com.innotech.management.model.Constants.StockTransactionType;

/**
 * @author dhaval
 * 
 */
@Entity
@View(members = "Stock Company  [companyCode; transactionType;dateOfPruchaseOrSale];"
		+ "Stock Value Info [quantity,faceValue;,commission,tax;total];" + "Account Info [dematAccount];")
@Tab(properties = "dematAccount.accountCode,dematAccount.firstName.name,companyCode.companyCode,companyCode.description,quantity+,total+,dateOfPruchaseOrSale")
public class Stock {

	/**
	 * 
	 */
	public Stock() {
		this.transactionType = StockTransactionType.Purchase;
	}

	@Id
	@Hidden
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(length = 32)
	private String id;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@DescriptionsList(order = "companyCode asc")
	@Required
	private Company companyCode;

	@Required
	@Column(length = 20)
	@Editor(value = "ValidValuesRadioButton")
	@OnChange(OnStockDetailsChange.class)
	private Constants.StockTransactionType transactionType;

	@Required
	@DefaultValueCalculator(CurrentDateCalculator.class)
	private Date dateOfPruchaseOrSale;

	@Column(length = 5)
	@Required
	@OnChange(OnStockDetailsChange.class)
	@DefaultValueCalculator(value = org.openxava.calculators.IntegerCalculator.class, properties = { @PropertyValue(name = "value", value = "0") })
	private int quantity;

	@Digits(fractionalDigits = 2, integerDigits = 5)
	@Required
	@DefaultValueCalculator(value = org.openxava.calculators.BigDecimalCalculator.class, properties = { @PropertyValue(name = "value", value = "0") })
	@OnChange(OnStockDetailsChange.class)
	private BigDecimal faceValue;

	@Digits(fractionalDigits = 2, integerDigits = 5)
	@Required
	@DefaultValueCalculator(value = org.openxava.calculators.BigDecimalCalculator.class, properties = { @PropertyValue(name = "value", value = "0") })
	@OnChange(OnStockDetailsChange.class)
	private BigDecimal commission;

	@Digits(fractionalDigits = 2, integerDigits = 5)
	@Required
	@DefaultValueCalculator(value = org.openxava.calculators.BigDecimalCalculator.class, properties = { @PropertyValue(name = "value", value = "0") })
	@OnChange(OnStockDetailsChange.class)
	private BigDecimal tax;

	@Digits(fractionalDigits = 2, integerDigits = 15)
	@DefaultValueCalculator(value = org.openxava.calculators.BigDecimalCalculator.class, properties = { @PropertyValue(name = "value", value = "0") })
	private BigDecimal total;

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@DescriptionsList(descriptionProperties = "accountCode,firstName.name ,firstName.familyGroup.description")
	@Required
	private DematAccount dematAccount;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the companyCode
	 */
	public Company getCompanyCode() {
		return companyCode;
	}

	/**
	 * @param companyCode
	 *            the companyCode to set
	 */
	public void setCompanyCode(Company companyCode) {
		this.companyCode = companyCode;
	}

	public Constants.StockTransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(Constants.StockTransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public Date getDateOfPruchaseOrSale() {
		return dateOfPruchaseOrSale;
	}

	public void setDateOfPruchaseOrSale(Date dateOfPruchaseOrSale) {
		this.dateOfPruchaseOrSale = dateOfPruchaseOrSale;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getFaceValue() {
		return faceValue;
	}

	public void setFaceValue(BigDecimal faceValue) {
		this.faceValue = faceValue;
	}

	public BigDecimal getCommission() {
		return commission;
	}

	public void setCommission(BigDecimal commission) {
		this.commission = commission;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public DematAccount getDematAccount() {
		return dematAccount;
	}

	public void setDematAccount(DematAccount dematAccount) {
		this.dematAccount = dematAccount;
	}

}
