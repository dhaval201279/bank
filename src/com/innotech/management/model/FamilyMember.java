/**
 * 
 */
package com.innotech.management.model;

import java.util.*;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.openxava.annotations.*;
import org.openxava.calculators.*;

/**
 * @author ds
 *
 */

@View(members = "Primary Info [familyGroup; "+" name ];  "
		+ " Other [gender,birthDate];"
		+ " Account Info (Optional) [pan];")
@Entity
@Tab(properties="name,birthDate,familyGroup.description")
public class FamilyMember {

	/**
	 * 
	 */
	public FamilyMember() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@Hidden
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid" , strategy="uuid")
	@Column(length=32)
	private String oid;
	 
	@Column(length=30) @Required
	private String name;
	    
	@Column(length = 10) @Required
	private Gender gender;

	@Required
	@DefaultValueCalculator(CurrentDateCalculator.class)
	private Date birthDate;
	
	
	@ManyToOne(fetch=FetchType.EAGER,optional=false)
	@DescriptionsList(order="description asc")
	@Required
	private FamilyGroup familyGroup;
	
	
	@Column(length=10) 
	private String pan;
	
	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the gender
	 */
	public Gender getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	/**
	 * @return the birthDate
	 */
	public Date getBirthDate() {
		return birthDate;
	}

	/**
	 * @param birthDate the birthDate to set
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	
	public FamilyGroup getFamilyGroup() {
		return familyGroup;
	}

	public void setFamilyGroup(FamilyGroup familyGroup) {
		this.familyGroup = familyGroup;
	}

	/**
	 * @return the pan
	 */
	public String getPan() {
		return pan;
	}

	/**
	 * @param pan the pan to set
	 */
	public void setPan(String pan) {
		this.pan = pan;
	}
	
	
}

enum Gender {
	MALE,
	FEMALE
};
