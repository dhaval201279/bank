<%@include file="../xava/imports.jsp"%>

<%@page import="java.util.Iterator"%>
<%@page import="org.openxava.application.meta.MetaModule"%>
<%@page import="org.openxava.util.Users"%>
<%@page import="com.openxava.naviox.Modules"%>

<jsp:useBean id="modules" class="com.openxava.naviox.Modules" scope="session"/>

<span style="float: left;">

<%
boolean showFirstTime = false;
for (Iterator it= modules.getTopModules().iterator(); it.hasNext();) {
	MetaModule module = (MetaModule) it.next();
	if (module.getName().equals("SignIn")) continue; 
	if (module.getName().equals("FirstSteps")) continue;
	String selected = module.getName().equals(request.getParameter("module"))?"selected":"";
	if(!showFirstTime){%>
		<a id="show_modules" href=""><xava:message key="all_modules"/></a>
	<% showFirstTime = true;}
%>	
<%--<a  href="/<%=module.getMetaApplication().getName()%>/m/<%=module.getName()%>" class="<%=selected%>">
		<%=module.getLabel(request.getLocale())%>
	</a>
	 --%>

<%
}
%>
</span>

<span style="float: right;">
<%
String userName = Users.getCurrent();
if (userName == null) {
%>
<% String selected = "SignIn".equals(request.getParameter("module"))?"selected":""; %>
<a href="<%=request.getContextPath()%>/m/SignIn" class="sign-in <%=selected%>">
		<xava:message key="signin"/>
</a>
<%
}
else {
%>
<a  href="<%=request.getContextPath()%>/naviox/signOut.jsp" class="sign-in"><xava:message key="signout"/> (<%=userName%>)</a>
<%
}
%>
</span>